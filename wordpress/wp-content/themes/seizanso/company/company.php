<?php
/*
  Template Name: Company Page
 */
get_header();
?>
<body>
  <?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <h2 class="ttl">法人概要<span>Company</span></h2>
    </div>
    <!--/.gr_ttl-->
    <?php the_content()?>
    <section class="st_profile">
      <div class="row wrap">
        <h2 class="ttl_profile"><span><?php the_field('ttl_profile')?></span></h2>
        <?php if( have_rows('list_profile')): ?>
          <ul class="list_profile">
            <?php while( have_rows('list_profile') ): the_row(); ?>
            <li>
              <span><?php the_sub_field('list_dt'); ?></span>
              <em><?php _e(nl2br(get_sub_field('list_dd'))) ?></em>
            </li>
          <?php endwhile;?>
          </ul>
        <?php endif;?>
        <!--/.list_profile-->
        <div class="btn_company">
          <a href="<?php the_field('btn_profile')?>">ブログ一覧へ</a>
        </div>
      </div>
    </section>
    <section class="st_history">
      <div class="row wrap">
        <h2 class="ttl_profile"><span><?php the_field('ttl_history')?></span></h2>
        <?php if( have_rows('list_history')): ?>
          <ul class="list_profile">
            <?php while( have_rows('list_history') ): the_row(); ?>
            <li>
              <span><?php the_sub_field('list_dt'); ?></span>
              <em><?php _e(nl2br(get_sub_field('list_dd'))) ?></em>
            </li>
          <?php endwhile;?>
          </ul>
        <?php endif;?>
        <!--/.list_profile-->
      </div>
    </section>
    <div class="map">
      <?php the_field('map')?>
    </div>
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>