<?php
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');
function enqueue_script(){
  wp_enqueue_script('libs',get_template_directory_uri().'/common/js/libs.js', '1.0', 1 );
  if(is_front_page()) {
    wp_enqueue_script('toppage', get_template_directory_uri().'/js/script.js', '1.0', 1 );
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_script');
function enqueue_style() {
  if(is_front_page()) {
    wp_enqueue_style('toppage',get_stylesheet_directory_uri().'/css/style.css');
  }
  if(is_page_template('company/company.php')) {
    wp_enqueue_style( 'company-style', get_stylesheet_directory_uri() . '/company/css/style.css');
  }
  if(is_page_template('recruit/recruit.php')) {
    wp_enqueue_style( 'recruit-style', get_stylesheet_directory_uri() . '/recruit/css/style.css');
  }
  if(is_page_template('news/news.php')) {
    wp_enqueue_style( 'news-style', get_stylesheet_directory_uri() . '/news/css/style.css');
  }
  if(is_page_template('facilities/facilities.php')) {
    wp_enqueue_style( 'facilities-style', get_stylesheet_directory_uri() . '/facilities/css/style.css');
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_style' );
function mp_pagination($prev = 'Prev', $next = 'Next', $pages='') {
    global $wp_query, $wp_rewrite, $textdomain;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }
    if(array_key_exists('pg',$_GET)){
      if($_GET['pg']) {
        $pg = $_GET['pg'];
      } else {
        $pg = 1;
      }
    } else {
      $pg = 1;
    }
    $big = 999999999;
    $pagination = array(
    'base'      => @add_query_arg( 'pg', '%#%' ),
    'format'   => '',
    'current'   => max ( 1, $pg),
    'total'   => $pages,
    'prev_text' => __($prev,$textdomain),
    'next_text' => __($next,$textdomain),
    'type'   => 'list',
    'end_size'  => 1,
    'mid_size'  => 2
    );
    $return =  paginate_links( $pagination );
    // echo str_replace( "<a class='page-numbers'>", '<a class="page_num">', $return );
    echo $return;
}
function action_get_3_post() {
  $arr = array(
    'post_type'      => 'post',
    'order'             => 'DESC',
    'orderby'           => 'id',
    'posts_per_page'    => 3
  );
  $query = new WP_Query($arr);
  return $query;
}
add_filter('get_3_post','action_get_3_post',10,1);
function action_news() {
  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
  $args = array(
    'post_type' => 'post',
    'orderby'   => 'id',
    'order'     => 'desc',
    'paged' => $paged,
    'posts_per_page' => 3
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_news','action_news',10,1);
remove_filter('template_redirect', 'redirect_canonical'); 
?>