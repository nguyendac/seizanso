<?php
  get_header();
?>
<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main>
      <div class="gr_ttl">
        <h2 class="ttl"><span>ページが見つかりません</span></h2>
      </div>
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>
  </body>
  </html>