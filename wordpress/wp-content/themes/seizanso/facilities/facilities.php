<?php
/*
  Template Name: Facilities Page
 */
get_header();
?>
<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <h2 class="ttl">施設一覧<span>Facilities</span></h2>
    </div>
    <!--/.gr_ttl-->
    <div class="gr_facilities">
      <div class="row">
        <div class="gr_facilities_top">
          <h3><span>施設・事業所</span></h3>
          <p><?php _e(nl2br(get_field('f_intro')))?></p>
        </div>
        <!--/.top-->
        <?php if( have_rows('fa')): ?>
        <div class="gr_facilities_list">
          <?php while( have_rows('fa') ): the_row(); ?>
          <article>
            <a href="<?php the_sub_field('fa_link'); ?>">
              <figure>
                <img src="<?php the_sub_field('fa_image'); ?>" alt="facilities1">
              </figure>
              <h4><?php _e(nl2br(get_sub_field('fa_title'))) ?></h4>
              <span>詳細ページへ</span>
            </a>
          </article>
          <?php endwhile; ?>
        </div>
        <?php endif;?>
        <!--/.list-->
      </div>
    </div>
    <!--/.gr_facilities-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>