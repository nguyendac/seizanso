<?php
/*
  Template Name: News Page
 */
get_header();
?>

<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <h2 class="ttl">お知らせ<span>News</span></h2>
    </div>
    <!--/.gr_ttl-->
    <div class="gr_news">
      <div class="row">
        <div class="news_item">
          <?php 
            $news = apply_filters('get_news','');
            $c = 0;
            while ($news->have_posts()) : $news->the_post();
            $c++;
          ?>
          <article id="news<?php _e($c)?>">
            <time datetime="<?php the_time('Y-m-d')?>"><?php the_time('Y.m.d')?></time>
            <h3><?php the_title(); ?></h3>
            <?php _e(nl2br(the_content()))?>
          </article>
          <?php endwhile;wp_reset_query(); ?>
        </div>
        <!--/.news_item-->
        <ul class="l_pag">
          <li><?php _e(get_previous_posts_link('前のページ'))?></li>
          <li><?php _e(get_next_posts_link('次のページ',$news->max_num_pages))?></li>
        </ul>
        <!--/.l_pag-->
      </div>
    </div>
    <!--/.gr_news-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>