<h1>
  <a href="<?php _e(home_url())?>">
    <picture>
      <source media="(max-width: 767px)" srcset="<?php bloginfo('template_url')?>/common/images/logo_sp.png">
      <img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="社会福祉法人青森民友厚生振興団">
    </picture>
  </a>
</h1>
<div class="header_main">
  <nav id="nav" class="nav">
    <ul class="nav_list">
      <li class="_top"><a href="<?php _e(home_url())?>">トップページ</a></li>
      <li><a href="<?php _e(home_url())?>/company">法人概要</a></li>
      <li><a href="<?php _e(home_url())?>/facilities">施設一覧</a></li>
      <li><a href="<?php _e(home_url())?>/recruit">スタッフ募集</a></li>
      <li><a href="<?php _e(home_url())?>/news">お知らせ</a></li>
    </ul>
    <ul class="nav_btn">
      <li><a href="" class="mail">お問い合わせ</a></li>
      <li><a href="tel:0173-35-4215" class="tel">0173-35-4215</a></li>
    </ul>
  </nav>
</div>
<div class="area_icon">
  <div class="menu_sp" id="icon_nav">
    <div class="icon_menu">
      <div class="icon_inner"></div>
    </div>
  </div>
</div>