<div class="footer_top">
  <div class="footer_banner row">
    <ul>
      <li><a href=""><img src="<?php bloginfo('template_url')?>/common/images/bn1.jpg" alt="bn 01"></a></li>
      <li><a href=""><img src="<?php bloginfo('template_url')?>/common/images/bn2.jpg" alt="bn 02"></a></li>
      <li><a href=""><img src="<?php bloginfo('template_url')?>/common/images/bn3.jpg" alt="bn 03"></a></li>
      <li><a href=""><img src="<?php bloginfo('template_url')?>/common/images/bn4.jpg" alt="bn 04"></a></li>
    </ul>
  </div>
</div>
<div class="footer_bottom">
  <div class="footer_bottom_l">
    <p>
      <picture>
        <source media="(max-width: 767px)" srcset="<?php bloginfo('template_url')?>/common/images/slogan_footer_sp.png">
        <img src="<?php bloginfo('template_url')?>/common/images/slogan_pc.png" alt="">
      </picture>
    </p>
    <address>〒037-0011 青森県五所川原市大字金山字盛山42-8</address>
    <a href="tel:0173-35-4215" class="tel">0173-35-4215</a>
  </div>
  <div class="footer_bottom_r">
    <ul>
      <li><a href="">サイトマップ</a></li>
      <li><a href="">プライバシーポリシー</a></li>
    </ul>
    <p>Copyright © 2018 SEIZAN-SO. All Rights Reserved.</p>
  </div>
</div>