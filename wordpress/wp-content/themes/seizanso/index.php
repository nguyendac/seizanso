<?php get_header();?>
<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main>
      <section id="fv" class="fv">
        <div class="fv_under" id="under">
          <ul>
            <li>
              <picture>
                <source media="(max-width: 767px)" srcset="<?php bloginfo('template_url')?>/images/slider_01_sp.jpg" />
                <img src="<?php bloginfo('template_url')?>/images/slider_01_pc.jpg" alt="test" />
              </picture>
            </li>
            <li>
              <picture>
                <source media="(max-width: 767px)" srcset="<?php bloginfo('template_url')?>/images/slider_01_sp.jpg" />
                <img src="<?php bloginfo('template_url')?>/images/slider_01_pc.jpg" alt="test" />
              </picture>
            </li>
          </ul>
        </div>
        <div class="fv_front">
          <p><img src="<?php bloginfo('template_url')?>/images/slogan.png" alt="slogan"></p>
        </div>
      </section><!-- end fv -->
      <section class="st_notice">
        <div class="row wrap">
          <h2 class="ttl_note">お知らせ</h2>
          <ul class="list_note">
            <?php
              $news = apply_filters('get_3_post','');
              $c = 0;
              while ($news->have_posts()) : $news->the_post();
              $c++;
            ?>
            <li>
              <a href="<?php _e(home_url())?>/news#news<?php _e($c)?>">
                <time datetime="<?php the_time('Y-m-d')?>"><?php the_time('Y.m.d')?></time>
                <em><?php the_title();?></em>
              </a>
            </li>
            <?php endwhile;wp_reset_query();?>
          </ul>
          <!--/.list_note-->
          <div class="btn btn_note">
            <a href="<?php _e(home_url())?>/news">お知らせ一覧へ</a>
          </div>
        </div>
      </section>
      <!--/.st_notice-->
      <section class="st_about">
        <div class="row wrap">
          <div class="gr_about_t">
            <div class="gr_about_t_left">
              <h2><span>青森民友厚生振興団</span>について</h2>
              <p>この文章はサンプルテキストです。<br>
              後ほど正式な文章と差し替えにな<br>
              ります。この文章はサンプルテキ<br>
              ストです。<br>
              後ほど正式な文章と差し替えにな<br>
              ります。後ほど正式な文章と差し<br>
              替えになります。</p>
            </div>
            <!--/.left-->
            <div class="gr_about_t_right">
              <figure><img src="<?php bloginfo('template_url')?>/images/campany.jpg" alt="campany"></figure>
            </div>
            <!--/.right-->
          </div>
          <!--/.gr_about-->
          <div class="btn btn_about">
            <a href="<?php _e(home_url())?>/company">お知らせ一覧へ</a>
          </div>
          <!--/.btn-->
        </div>
        <div class="row wrap">
          <div class="gr_about_bt">
            <div class="gr_about_bt_left bx_w">
              <figure>
                <span>施設一覧</span>
                <a href="<?php _e(home_url())?>/facilities"><img src="<?php bloginfo('template_url')?>/images/facilities.jpg" alt="facilities"></a>
                <figcaption>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。文字の大きさ、量、字間、行間等を確認するために入れています。</figcaption>
              </figure>
            </div>
            <!--/.left-->
            <div class="gr_about_bt_right bx_w">
              <figure>
                <span>採用情報</span>
                <a href="<?php _e(home_url())?>/recruit"><img src="<?php bloginfo('template_url')?>/images/staff.jpg" alt="staff"></a>
                <figcaption>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。文字の大きさ、量、字間、行間等を確認するために入れています。</figcaption>
              </figure>
            </div>
            <!--/.right-->
          </div>
        </div>
      </section>
      <!--/.st_about-->
      <section class="st_blog">
        <div class="row wrap">
          <h2 class="ttl_blog">ブログ</h2>
          <div class="gr_blog">
            <?php
              //以下3行の項目を任意に変更
                $display_posts_count = 3; //実際に表示したい記事件数
                $get_posts_count = 3; //取得する記事件数（PR記事を含むので$display_posts_countより少し多めに設定）
                $feed = fetch_feed('http://feedblog.ameba.jp/rss/ameblo/seizanso4215/rss20.xml'); //取得したいRSS
              //
                $counter = 0; //ループ回数カウンター
                include_once(ABSPATH . WPINC . '/feed.php');
                if (!is_wp_error( $feed ) ) : //エラーがなければ
                    date_default_timezone_set('Asia/Tokyo'); //タイムゾーンを日本時間に設定
                     $maxitems = $feed->get_item_quantity($get_posts_count); //取得件数
                     $feed_items = $feed->get_items(0, $maxitems); //指定件数分の配列作成
                endif;
              ?>
              <?php
                if($feed_items > 0):
                  foreach ( $feed_items as $item ):
                    $item->thumbnail = get_bloginfo("template_url") . "/common/images/noimage.png";
                    // 記事の中で最初に使われている画像を検索、設定する
                    if( preg_match_all('/<img(.+?)>/is', $item->get_description(), $matches) ){
                      foreach( $matches[0] as $img ){
                        if( preg_match('/src=[\'"](.+?jpe?g)[\'"]/', $img, $m) ){
                          $item->thumbnail = $m[1];
                        }
                      }
                    }
              ?>
            <article>
              <a href="<?php _e($item->get_permalink());?>" target="_blank">
                <figure>
                  <img src="<?php _e($item->thumbnail)?>" alt="Blog">
                </figure>
                <div class="main_art">
                  <time datetime="<?php _e($item->get_date('Y-m-d'))?>"><?php echo $item->get_date('Y.m.d'); ?></time>
                  <h3><?php _e($item->get_title());?></h3>
                  <p><?php _e(mb_substr(strip_tags($item->get_description()),0,50));?>...<span class="more" href="#">続きはこちら</span></p>
                </div>
                <!--/.main_art-->
              </a>
            </article>
            <?php $counter++;endforeach;endif;?>
          </div>
          <!--/.gr_blog-->
          <div class="btn_blog">
            <a href="https://ameblo.jp/seizanso4215/" target="_blank">ブログ一覧へ</a>
          </div>
        </div>
      </section>
      <!--/.st_blog-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div><!-- end container -->
  <?php get_footer();?>
</body>
</html>