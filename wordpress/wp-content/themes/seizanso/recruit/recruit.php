<?php
/*
  Template Name: Recruit Page
 */
get_header();
?>
<body>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <h2 class="ttl">採用情報<span>Recruit</span></h2>
    </div>
    <!--/.gr_ttl-->
    <div class="gr_recruit">
      <div class="row">
        <article>
          <h3><?php the_field('ttl_sec_1')?></h3>
          <p><?php _e(nl2br(get_field('intro_sec_1')))?></p>
          <?php if( have_rows('list_sec_1')): ?>
            <ul class="list_profile">
              <?php while( have_rows('list_sec_1') ): the_row(); ?>
              <li>
                <span><?php the_sub_field('list_dt'); ?></span>
                <em><?php _e(nl2br(get_sub_field('list_dd'))) ?></em>
              </li>
            <?php endwhile;?>
            </ul>
          <?php endif;?>
          <div class="btn_company">
            <a href="<?php the_field('btn_sec_1')?>">お問い合わせ</a>
          </div>
        </article>
        <article>
          <h3><?php the_field('ttl_sec_2')?></h3>
          <p><?php _e(nl2br(get_field('intro_sec_2')))?></p>
          <?php if( have_rows('list_sec_2')): ?>
            <ul class="list_profile">
              <?php while( have_rows('list_sec_2') ): the_row(); ?>
              <li>
                <span><?php the_sub_field('list_dt'); ?></span>
                <em><?php _e(nl2br(get_sub_field('list_dd'))) ?></em>
              </li>
            <?php endwhile;?>
            </ul>
          <?php endif;?>
          <div class="btn_company">
            <a href="<?php the_field('btn_sec_2')?>">お問い合わせ</a>
          </div>
        </article>
      </div>
    </div>

    <!--/.gr_recruit-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>