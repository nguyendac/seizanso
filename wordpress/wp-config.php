<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'seizanso');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CPoNO4(H2&176ff266vM^XM5UypYdD[JGbj.-BlmZ(PnJ?/TFx-bWaRS#JxoZC[L');
define('SECURE_AUTH_KEY',  '8TRO`4z0e9wn5]XgaHG|xCYGG`J_@X!_Rt<v~?{h):RbQOv ?1Rs_/u;P2 Gdc`O');
define('LOGGED_IN_KEY',    '/nEHhB9QrAs|7hr<A|&[cPUC;}w_*addqqR:T6a7n(<`c=nEEzdYeL.gsEGnZ!Jh');
define('NONCE_KEY',        '_Wq74p:oE oJ!!SYG@-DOu}Ze7h<j:8|9G_<M0ej)+ytV.kx^B3shQ4:z1A@NzW*');
define('AUTH_SALT',        ' ,CkH[)B9`~L{^M+7WDdLt+VMJ_XRA`{rA7Np+?$d-.[Kaol`/@+?uP_Hm0S@#<`');
define('SECURE_AUTH_SALT', 'GmH=PuEky$]!a6W?:!F}Xc/%c3Z$}7V7~8[X#fy&X Ch0R}z?MIb2-8y5*QY4-`c');
define('LOGGED_IN_SALT',   'E04T?CJ{SAf(e;}hNY%TlyF[^S[1UIi7$D0n9v[x#;->)d8 @&pf/(hg{w|R;[fC');
define('NONCE_SALT',       'dkHTe.!kW@bEcKHy8HaR&UH}#`3+3Hg[fL9g)VK3P5TvM.Rv)~5J-#ZHi eX(z,:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
